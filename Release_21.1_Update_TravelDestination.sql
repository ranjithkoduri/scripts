  /**
  <Author>Ranjith K</Author><Description>Correcting travel destination</Description><Date>05/08/2019</Date>
  **/
  
  DECLARE @ID INT
  SELECT @ID = Id FROM [dbo].[TravelandMedicalDestinationMapping] WITH (NOLOCK)  WHERE TravelDestination = 'Whole Europe' AND TravelUnderwriterId = 377 and TravelTripTypeId = 2  
  UPDATE [dbo].[TravelandMedicalDestinationMapping] set TravelDestination = 'Whole of Europe' where Id = @ID

  SELECT @ID = Id FROM [dbo].[TravelandMedicalDestinationMapping] WITH (NOLOCK)  WHERE TravelDestination = 'Whole Europe' AND TravelUnderwriterId = 377 and TravelTripTypeId = 1  
  UPDATE [dbo].[TravelandMedicalDestinationMapping] set TravelDestination = 'Whole of Europe' where Id = @ID