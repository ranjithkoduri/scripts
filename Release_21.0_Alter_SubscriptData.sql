/*
<Author>Ranjith K</Author><Date>30/07/2019</Date><Description>Increased column to max</Description>
*/
ALTER TABLE [dbo].[SubScript] ALTER COLUMN [SubScriptData] VARCHAR (max)

