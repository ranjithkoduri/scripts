/**
<Author>Ranjith K</Author><Description>Update script for application id for autorenewal quotes</Description><Date>24-07-2019</Date>
**/
update dbo.UserPreset set ApplicationId = 1 where serviceagentname = 'AutoRenew' and DateCreated > '2019-06-05'    