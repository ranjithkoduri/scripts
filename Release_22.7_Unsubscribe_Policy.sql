INSERT INTO dbo.Unsubscribe (
	PolicyNumber
	,EmailAddress
	,[Month]
	,CreatredDate
	,OptoutReasonId
	,Notes
	,IsActive
	)
SELECT P.PolicyNumber
	,PH.EmailAddress
	,MONTH(GETDATE()) AS Month
	,GETDATE() AS CreatedDate
	,9 AS OptoutReasonId
	,'Force optout by system' AS Notes
	,1 AS IsActive
FROM dbo.Policy P WITH (NOLOCK)
INNER JOIN DBO.PolicyHolder PH WITH (NOLOCK) ON PH.PolicyHolderId = P.PolicyHolderId
LEFT JOIN DBO.Unsubscribe U WITH (NOLOCK) ON U.PolicyNumber = P.PolicyNumber
WHERE P.PolicyNumber IN (
		'LGA16200852'
		,'LGX15543934'
		,'LGA16222065'
		)
	AND U.UnsubscribeId IS NULL