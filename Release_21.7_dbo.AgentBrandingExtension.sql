/****** Object:  Table [dbo].[AgentBrandingExtension]    Script Date: 9/18/2019 2:49:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgentBrandingExtension](
	[AgentBrandingExtensionId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AgentBrandingId] [int] NOT NULL,
	[EndorsementCodeName] [varchar](50) NOT NULL,
	[CallCentreEndorsementDescription] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastModified] [smalldatetime] NOT NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_AgentBrandingExtension] PRIMARY KEY CLUSTERED 
(
	[AgentBrandingExtensionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[AgentBrandingExtension] ALTER COLUMN [CallCentreEndorsementDescription] VARCHAR (max)